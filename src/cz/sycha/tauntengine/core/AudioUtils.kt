package cz.sycha.tauntengine.core

import net.dv8tion.jda.entities.VoiceChannel
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

class AudioUtils {
    fun findChannel(name: String, voiceChannels: List<VoiceChannel>): VoiceChannel {
        var found: Boolean = false

        for(channel: VoiceChannel in voiceChannels) {
            if(channel.name.equals(name, ignoreCase = true)) {
                found = true
                return channel
            }
        }
        throw ChannelNotFoundException()
    }

    fun randomFile(): File {
        var codeList: ArrayList<File> = ArrayList()

        var toReturn: File

        // This shit ain't work :(
        /*Files.walk(Paths.get("taunts")).forEach{ filePath -> {
            if(Files.isRegularFile(filePath)) {
                if(filePath.getFileName().endsWith(".mp3")) {
                    pathList.add(filePath)
                }
            }
        }}*/

        for(fileEntry: File in File("taunts").listFiles()) {
            if(fileEntry.isFile) {
                if(fileEntry.name.endsWith(".mp3") or fileEntry.name.endsWith(".wav")) {
                    codeList.add(fileEntry)
                }
            }
        }

        val rand: Random = Random()

        val number: Int = rand.nextInt(codeList.size)

        return codeList[number]
    }
}

class ChannelNotFoundException : Throwable() {  }
