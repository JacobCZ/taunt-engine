package cz.sycha.tauntengine.core

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import net.dv8tion.jda.JDA
import net.dv8tion.jda.JDABuilder
import net.dv8tion.jda.audio.player.FilePlayer
import net.dv8tion.jda.audio.player.Player
import net.dv8tion.jda.entities.VoiceChannel
import net.dv8tion.jda.events.ReadyEvent
import net.dv8tion.jda.events.message.MessageReceivedEvent
import net.dv8tion.jda.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.hooks.ListenerAdapter
import org.apache.commons.lang3.StringEscapeUtils
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.io.Reader
import java.net.URL
import javax.sound.sampled.UnsupportedAudioFileException

data class Joke(val joke: String)

class Main : ListenerAdapter() {
    private val gson: Gson = GsonBuilder().create()
    private var player: Player = FilePlayer()

    fun getRandomJoke(): String {
        val reader: Reader = InputStreamReader(URL("http://tambal.azurewebsites.net/joke/random").openStream())
        val obj = gson.fromJson(reader, Joke::class.java)
        return obj.joke
    }

    fun run(): Unit {
        val jda: JDA = JDABuilder("jakubsycha+bot@gmail.com", "Google.cz1").buildBlocking()
        jda.addEventListener(this)
    }

    override fun onReady(event: ReadyEvent) {
        println("System ready!")
    }

    override fun onMessageReceived(event: MessageReceivedEvent): Unit {
        val isPrivate: Boolean = event.isPrivate

        // If the message is private, just ignore it
        if(!isPrivate) {


            try {
                // Check whether it is a sharp-command
                val cmdarray: CharArray = event.message.content.toCharArray()
                if(cmdarray[0].equals('#')) {
                    // If it indeed is a sharp-command
                    val command = event.message.content.replace("#", "")

                    when(command) {
                        "help" -> {
                            event.channel.sendMessage("Yo! I'm the __**Taunt Engine**__ bot! You can see my command list down below!")
                            event.channel.sendMessage(
                                    "**#help**  -->  __Displays this help. No shit yo!__\n" +
                                    "**#joke**  -->  __Tells a randon joke. \"random\"__" +
                                    "__**Admin commands (can be issued only by admins)**__:\n"+
                                    "**#join [channel-name]** --> Join the specified voice channel\n" +
                                    "**#leave**  -->  Disconnects from voice channel"
                            )
                        }
                        "joke" -> {
                            val joke: String = StringEscapeUtils.unescapeHtml4(getRandomJoke())
                            event.channel.sendMessage("**$joke**")
                        }
                        "leave" -> {
                            event.jda.audioManager.closeAudioConnection()
                            event.channel.sendMessage("Disconnected from Audio channel!")
                        }
                        "taunt" -> {
                            if(player.isPlaying) {
                                event.channel.sendMessage("A taunt is already playing, please wait for it to finish...")
                            }
                            else {
                                var audioFile: File

                                try {
                                    //audioFile = File("t2.mp3")
                                    audioFile = AudioUtils().randomFile()
                                    player = FilePlayer(audioFile)
                                    event.jda.audioManager.setSendingHandler(player)
                                    event.channel.sendMessage("Now playing: ${audioFile.name}")
                                    player.play()
                                }
                                catch(e: IOException) {
                                    event.channel.sendMessage("Could not load file. (32)")
                                    e.printStackTrace()
                                }
                                catch(e: UnsupportedAudioFileException) {
                                    event.channel.sendMessage("Could not load file. (11)")
                                    e.printStackTrace()
                                }
                            }
                        }
                        "stop" -> {
                            if(event.author.username.equals("Jac0bas")) {
                                if(!player.isPlaying) {
                                    event.channel.sendMessage("Nothing is playing!")
                                    return
                                }
                                else {
                                    player.stop()
                                    event.channel.sendMessage("Stopped!")
                                }
                            }
                            else {
                                event.channel.sendMessage("Only admins can use this command!")
                            }
                        }
                    }

                    // The {#join [channel-name]} command
                    if(command.startsWith("join ")) {
                        if(!event.author.username.equals("Jac0bas")) {
                            event.channel.sendMessage("Only admins can use this command!")
                            return
                        }

                        val chanName: String = command.substring(5)

                        try {
                            val channel: VoiceChannel = AudioUtils().findChannel(chanName, event.guild.voiceChannels)
                            event.jda.audioManager.openAudioConnection(channel)
                            event.channel.sendMessage("Connected to voice channel!")
                        }
                        catch(e: ChannelNotFoundException) {
                                event.channel.sendMessage("No channel with that name! *(Usage: #join [channel-name])*")
                        }
                        catch(e: IllegalStateException) {
                            event.channel.sendMessage("The bot is already in a channel. Use *#leave* first if you want to change it...")
                        }
                    }
                }

                // Ignore any other messages
            }
            catch(e: NullPointerException) {
                println("Error, this message could not be received. Sorry.")
            }
        }
    }
}

fun main(args: Array<String>): Unit {
    Main().run()
}